const express    = require('express')
const app        = express()
const bodyParser = require('body-parser')
const mongoose   = require('mongoose')
const cors       = require('cors')

mongoose.connect('mongodb://localhost/blockchain-test', (err)=>{
    if(err){
        console.log('mongo Error '+ err)
    } else{
        console.log('MONGO CONNECTED')
    }
})



// ETHEREUM STUFF
const admin = require('firebase-admin');
const Web3 = require('web3');
const fs = require('fs');
const path = require('path');
const jsonPath = path.join(__dirname, '..', 'ethereum', 'SecureIdFactory.json');
// const secureIdFactoryABI = fs.readFileSync('./ethereum/SecureIdFactory.json', 'utf8'); //taro di env
const secureIdFactoryABI = require('./ethereum/SecureIdFactory.json')
const Contract = require('truffle-contract');
const Promise = require('bluebird')
var CryptoJS = require('crypto-js');
// var keythereum = require("keythereum");
var coder = require('web3/lib/solidity/coder');
var Transaction = require('ethereumjs-tx');
// var serviceAccount = require(path.join(__dirname, '..', 'serviceAccount.json'));
var serviceAccount = require('./serviceAccount.json')

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount)
});

const db = admin.firestore();
var provider = new Web3.providers.HttpProvider("http://kartoonh7.eastus.cloudapp.azure.com:8545");
var web3 = new Web3(provider);
web3Eth = Promise.promisifyAll(web3.eth)

// secure ID
// var secureIdFactory = Contract(secureIdFactoryABI);
// secureIdFactory.setProvider(provider);

// secureIdFactory.deployed()
//     .then(instance => {
//         console.log(instance.address)
//         var smth = new instance.getSecureIdInfo()
//         console.log(smth)
//     })
//     .catch(err =>{
//         console.log('error blockchain '+ err)
//     })

db.collection('users')
    .doc('bZCbqYhdnNZaXzCXAX6GN9llAjD3')
    .get()
    .then(doc => {
        if(doc){
            // console.log(doc.data())
        }
    })


// SERVER STUFF

app.use(bodyParser.urlencoded({extended:false}))
app.use(bodyParser.json())
app.use(cors())

const user  = require('./routes/user')
const batch = require('./routes/batch')


app.use('/v1/user', user)
app.use('/v1/batch', batch)

app.get('/', (req,res)=>{
    console.log('PORT 3000')
    // secureIdFactory.deployed()
    //     .then(instance => {
    //         console.log(instance.address)
    //         var smth = new instance.getSecureIdInfo()
    //         console.log(smth)
    //     })
    //     .catch(err => {
    //         console.log('error blockchain ' + err)
    //     })
    res.send({
        name: 'TEST',
        text: 'HELLO WORLD'
    })
})

app.listen(3000,(err)=>{
    if(!err)
    console.log('PORT 3000')
    // console.log(secureIdFactoryABI)
    // console.log(
    //     admin.initializeApp({
    //         credential: admin.credential.cert(serviceAccount)
    //     })
    // )
        // console.log(web3Eth)
        console.log(path.join(__dirname, '..', 'ethereum', 'SecureIdFactory.json'))
})

module.exports = {
    // app:'app',
    web3Eth:'alfahf'
}