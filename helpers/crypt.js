var CryptoJS = require('crypto-js');
var coder    = require('web3/lib/solidity/coder');

function encodeFunctionTxData(functionName, types, args) {
    var fullName = functionName + '(' + types.join() + ')';
    var signature = CryptoJS.SHA3(fullName, { outputLength: 256 }).toString(CryptoJS.enc.Hex).slice(0, 8);
    var dataHex = signature + coder.encodeParams(types, args);
    return dataHex;
}

module.exports = encodeFunctionTxData