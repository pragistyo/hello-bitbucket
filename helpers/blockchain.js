const Web3     = require('web3');
const Promise  = require('bluebird')
const provider = new Web3.providers.HttpProvider("http://kartoonh7.eastus.cloudapp.azure.com:8545");

var web3       = new Web3(provider);
web3.eth       = promisifyAll(web3.eth)

function getTransactionReceiptMined(txHash, interval) {
    const self = this;
    const transactionReceiptAsync = function (resolve, reject) {
        web3.eth.getTransactionReceipt(txHash, (error, receipt) => {
            if (error) {
                reject(error);
            } else if (receipt == null) {
                setTimeout(
                    () => transactionReceiptAsync(resolve, reject),
                    interval ? interval : 500);
            } else {
                resolve(receipt);
            }
        });
    };

    if (Array.isArray(txHash)) {
        return Promise.all(txHash.map(
            oneTxHash => self.getTransactionReceiptMined(oneTxHash, interval)));
    } else if (typeof txHash === "string") {
        return new Promise(transactionReceiptAsync);
    } else {
        throw new Error("Invalid Type: " + txHash);
    }
};

function promiseGetLastSecureId(instance, address) {
    return new Promise(function (resolve, reject) {
        instance.getLastSecureId.call(address).then(function (secureId) {
            if (secureId) {
                resolve(secureId);
            } else {
                reject(secureId);
            }
        })
    })
}

function promisedSendRawTransaction(serializedTx) {
    return new Promise(function (resolve, reject) {
        web3.eth.sendRawTransaction(serializedTx, function (e, data) {
            if (e !== null) {
                console.log(e);
                reject(e);
            } else {
                resolve(data);
            }
        });
    });
};

module.exports = {
    getTransactionReceiptMined,
    promiseGetLastSecureId,
    promisedSendRawTransaction
}