const UserModel  = require('../models/user')
var keythereum   = require('keythereum')
const bcrypt     = require('bcrypt')
const saltRounds = 10;
const Web3       = require('web3')
const Contract   = require('truffle-contract');
var provider = new Web3.providers.HttpProvider("http://kartoonh7.eastus.cloudapp.azure.com:8545");
const secureIdFactoryABI = require('../ethereum/SecureIdFactory.json')
const Promise = require('bluebird')
const web3Eth = require('../app.js')


console.log(web3Eth)


class User {
    constructor(){

    }

    static testCreate(req, res, next){
        // res.send(req.body)
        var dk = keythereum.create({ keyBytes: 32, ivBytes: 16 });
        var uid = uid
        
        
        var options = {
            kdf: "pbkdf2",
            cipher: "aes-128-ctr",
            kdfparams: {
                c: 262144,
                dklen: 32,
                prf: "hmac-sha256"
            }
        };
        
        var keyObject = keythereum.dump('uid', dk.privateKey, dk.salt, dk.iv, options);

        var privateKey = keythereum.recover('uid', keyObject)
        var secureIdFactory = Contract(secureIdFactoryABI)
        secureIdFactory.setProvider(provider);
        // res.send(dk.privateKey)
        var send = {
            dkPrivate: JSON.stringify(dk.privateKey),
            resultPrivateKey : JSON.stringify(privateKey),
            secureIdFactory: secureIdFactory
        }
        secureIdFactory.deployed().then(instance => {
            let nonce = 
            // console.log(secureIdFactory)
            console.log('instance')
            res.send(instance)
        })
        .catch( err => {
            console.log('secureIdFactory ', err)
            res.send( 'err' )
        })

    }

    static getAllUser(req,res){
        UserModel.find()
        .then(result =>{
            res.send(result)
        })
        .catch(err => {
            console.log(err)
        })
    }

    static createUser(req,res){
        let hash = bcrypt.hashSync(req.body.password, saltRounds);
        // res.send(hash)
        UserModel.create({
            username: req.body.username,
            password: hash,
            company: req.body.company,
            firstname: req.body.firstname,
            lastname: req.body.lastname
        })
        .then( result =>{
            res.status(200).send(result)
        })
        .catch( err => {
            res.status(400).send(err)
        })
    }

    static singleUser(req,res){
        UserModel.findOne({_id:req.params.userId})
        .then( result => {
            res.status(200).send(result)
        })
        .catch( err =>{
            res.status(400).send(err)
        })
    }

    static updateUser(req,res){
        UserModel.findOneAndUpdate(
            {_id:req.params.userId},
            {
                $set: {
                    username:req.body.username,
                    company: req.body.company,
                    firstname: req.body.firstname,
                    lastname: req.body.lastname
                }
            },
            {new:true}
        )
        .then( result =>{
            res.status(200).send(result)
        })
        .catch(err => {
            res.status(400).send(err)
        })
    }

    static removeUser(req,res){
        UserModel.findByIdAndRemove({_id:req.params.userId})
        .then( result =>{
            res.status(200).send(result)
        })
        .catch(err => {
            res.status(400).send(err)
        })
    }

}

module.exports = User