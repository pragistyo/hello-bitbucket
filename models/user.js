const mongoose = require('mongoose')
const Schema = mongoose.Schema

var userSchema = new Schema({
    username: {type:String, unique:true, required:true},
    password: {type:String, unique:true, required:true},
    company: { type: String, unique: true, required: true },
    firstname: { type: String, required:true},
    lastname: {type: String, required:true}
}, {timestamps:true})

const user = mongoose.model('user', userSchema)
module.exports = user