const express = require('express')
const router  = express.Router()
const userCtrl = require('../controllers/user')

router.get('/', userCtrl.getAllUser)
// router.post('/', userCtrl.createUser)
router.get('/:userId', userCtrl.singleUser)
router.patch('/:userId', userCtrl.updateUser)
router.delete('/:userId', userCtrl.removeUser)

//dev test
router.post('/tes', userCtrl.testCreate)


module.exports = router